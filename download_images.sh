#!/bin/bash
# Script name: download_images.sh
#
# USAGE:
#   ./download_images.sh
# GLOBALS:
#   NULL
# ARGUMENTS:
#   NULL
# RETURN:
#   0 if success, non-zero otherwise

set -eufo pipefail
IFS=$'\n\t'

#### FUNCTION BEGIN
# Downloads URL passed and compares hash of downloaded file
# GLOBALS:
#   NULL
# ARGUMENTS:
#   Image URL
#   Image sha256 hash
# OUTPUTS:
#   Download progress when downloading images or
#   error with image hash comparison
# RETURN:
#   0 if success, non-zero otherwise
#### FUNCTION END
function download_image {
  image_url="${1:?}"
  image_hash="${2:?}"
  image_base="${image_url##*/}"

  if [[ ! -d "./downloads" ]]; then
    mkdir downloads
  fi

  if [[ ! -e "./downloads/${image_base}" ]]; then
    echo "Downloading image: ${image_base}"
    curl "${image_url}" --output "./downloads/${image_base}"
  fi

  image_local_hash=$(sha256sum "./downloads/${image_base}" | cut -d " " -f 1)

  if [[ "${image_local_hash}" != "${image_hash}" ]]; then
    echo "Error with hash of: ${image_base}"
    return 1
  fi
  return 0
}

#### FUNCTION BEGIN
# Starts the image downloads from images.txt
# GLOBALS:
#   NULL
# ARGUMENTS:
#   NULL
# OUTPUTS:
#   Error is images def is not found
# RETURN:
#   0 if success, non-zero otherwise
#### FUNCTION END
function process_images {
  if [[ ! -e ./images.txt ]]; then
    echo "No images file found in local dir"
    return 1
  fi
  while read -r image; do
    download_image "${image%%::*}" "${image##*::}"
  done <images.txt
  return 0
}

process_images

