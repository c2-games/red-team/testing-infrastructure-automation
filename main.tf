variable SSH_AUTH_KEY {
  description = "SSH authorized key"
}

variable FIRMWARE_PATH {
  description = "UEFI firmware path"
}

variable DEFAULT_DISK_SIZE {
  description = "Default disk size in bytes"
  type = number
  default = 8589934592
}

variable USERNAME {
  description = "Username provisioned on VM"
  default = "ansible"
}

variable vm_definition {
  description = ""
  type = map
  default = {
    ubuntu18 = {
      NAME = "c2_ubuntu18"
      Q2_SOURCE = "downloads/bionic-server-cloudimg-amd64.img"
    },
    ubuntu20 = {
      NAME = "c2_ubuntu20"
      Q2_SOURCE = "downloads/focal-server-cloudimg-amd64.img"
    },
    ubuntu22 = {
      NAME = "c2_ubuntu22"
      Q2_SOURCE = "downloads/jammy-server-cloudimg-amd64.img"
    },
    cent7 = {
      NAME = "c2_cent7"
      Q2_SOURCE = "downloads/CentOS-7-x86_64-GenericCloud.qcow2"
    },
    rocky8 = {
      NAME = "c2_rocky8"
      Q2_SOURCE = "downloads/Rocky-8-GenericCloud-Base.latest.x86_64.qcow2"
      DISK_SIZE = "17179869184"
    },
    rocky9 = {
      NAME = "c2_rocky9"
      Q2_SOURCE = "downloads/Rocky-9-GenericCloud-Base.latest.x86_64.qcow2"
      DISK_SIZE = "17179869184"
    }
  }
}

module "libvirt_cloud_init" {
  source = "git::https://gitlab.com/c2-games/infrastructure/terraform/libvirt-cloud-init.git?ref=main"

  for_each = var.vm_definition
  NAME = each.value.NAME
  Q2_SOURCE = each.value.Q2_SOURCE
  DISK_SIZE = try(each.value.DISK_SIZE, var.DEFAULT_DISK_SIZE)
  USERNAME = var.USERNAME
  SSH_AUTH_KEY = var.SSH_AUTH_KEY

  KERNEL_OVERWRITE = try(each.value.KERNEL_OVERWRITE, null)
}

