#!/bin/bash

set -eufo pipefail
IFS=$'\n\t'

function help {
  grep "^function" <"${0}" | cut -d " " -f 2
}

function checkRoot {
	if [[ ${EUID} -ne 0 ]]; then
		echo "Calling user is not root"
		exit 1
	fi
}

function show-vm-ips {
  jq -r  '["ID       ", "NAME"],
          ["---------","----"],
          (.resources[] | select(.type=="libvirt_domain") |
            [.instances[0].attributes.name, .instances[0].attributes.network_interface[].addresses[0]] ) | @tsv' <terraform.tfstate
}

function show-vms {
  jq -r  '.resources[] | select(.type=="libvirt_domain") |
            [.instances[0].attributes.name] | @tsv' <terraform.tfstate
}

function ansible-inv {
  vm_address="$(show-vm-ips | tail -n+3 | sed 's|\t| |g' | tr -s " ")"
  cat <<EOF
---
all:
  hosts:
EOF
  while read -r vms; do
    echo -e "    ${vms%% *}:\n      ansible_host: ${vms##* }"
  done <<<"${vm_address}"
  echo "..."
}

function prov-target {
  vm_address="$(show-vm-ips | tail -n+3 | sed 's|\t| |g' | tr -s " ")"
  echo -n "set job!targets "
  while read -r vms; do
    echo -n "${vms##* } "
  done <<<"${vm_address}"
  echo ""
}

function setup-snapshots {
  checkRoot
  vms="$(show-vms)"
  while read -r vm; do
    echo "Creating snapshot for ${vm}"
    if [[ -z "$(virsh snapshot-list --name "${vm}")" ]]; then
      virsh snapshot-create-as "${vm}" --name Base >/dev/null
    else
      echo "Snapshot already created for: ${vm}"
    fi
  done <<<"${vms}"
}

function restore-snapshots {
  checkRoot
  vms="$(show-vms)"
  while read -r line;
  do
    virsh snapshot-revert "${line}" --snapshotname Base \
      && echo "Reverted Snapshot for: ${line}"\
      || echo "Failed to revert Snapshot for: ${line}"
  done <<EOF
$vms
EOF

}

"${@:-help}"

